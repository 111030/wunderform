# WunderForm
=============================================================

This single-page application is a registration form template, with four steps (views) structured in one so that the user has a more beautiful UI experience and less waiting time on switching forwards and backwards.

It has the following structure:
*   controllers 
    *   personal_info_controller.php
    *   street_address_controller.php 
    *   bank_account_controller.php
    *   delete_from_customer_db.php
    *   update_and_finish.php
    *   session_getter.php 
    *   session_setter.php
*   css
    * main.css
*   js 
    * ajax.js
    * jquery-3-3-1.min.js
*   main.php 
*   wunder.sql 


## The Workflow
---

As you can see from the diagram above, my application is a **single page** web application which has way more UI benefits rather than an application with separate views for each scenario.
I will explain those parts briefly in the following section.

### Controllers
---

The controllers are .php files that serve as a connection between the main view and the database. 
When the user starts the application, the first controller that gets called is **session_getter.php** that gets the latest view (page) number that the user left within the session, so he is redirected there.

The **session_setter.php** controller accordingly to the previous one, writes the page number into the session each time the user navigates forwards and backwards.

While navigating forwards, each of the **personal_info_controller.php**, **street_address_controller.php** and **bank_account_controller.php** controllers gets called respectively to write the state of those form fields respectively in the session, so when the user leaves the page and comes back (but clicking 'Next' before) his data gets populated.

Moreover, the **bank_account_controller.php** is the last step in the registration process, so when clicking on 'Finish', a request to the endpoint https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data is being made. If the request was unsuccessful, the customer information is deleted from the database and the user gets notified about it through the controller **delete_from_customer_db.php**. 
Otherwise, the controller **update_and_finish.php** is being called which redirects the user to the success page and clears the session.


### Models
---
Altough this application doesn't explicity has a folder named **Models** and respective classes with property mappings to a database, the state of it is maintained in different way.
Becase I need a **Session** mechanism for keeping the user's data temporary until it gets written to the database, I use a **session variables** that get filled each time the user navigates forwards.
In particular, when the user leaves the first screen, the **SESSION['first_name'], SESSION['last_name'] and SESSION['telephone']** variables gets filled with the values from the form fields.


### Views
---
Because of its **SPA** nature, my application has only one view... the **main.php** view, that must passed in the url field when starting the web application.
Instead of navigating to different view each time, I just show the form that is intented for the current screen index, and hide the others. Each click on the 'Previous' and the 'Next' or the 'Finish' button, sends a request to the my local .php controllers.
When going back from another endpoing without turning the browser off, each field in the particular form gets prepopulated with the session values. 




## !!!Imprortant - I got cross origin exception when connection to the server endpoint so I wasn't able to reach the final stage


### My architecture and the things I would have done better

As you can see, the only thing (not really) missing from the whole MVC pattern is the **Models** part.
In fact, the models are hidden behind my **SESSION** variables. At first, my intention was to create a separate models folder with separate models for each entity (personal info, address, bank account) for convenience, but I would to that in near future when working with **Laravel** since it hase more MVC like architecture.

Another thing that I would have done better is using also MVC framework on frontend side like Angular, so I can keep everything like sessions and state management on frontend, and then I would only have used PHP acting like a proxy between it and the database.
But again, my intention was to use mainly PHP and to broad my knowledge in it through coding, so have fun with the application and don't hesitate to give me a feedback on it.
