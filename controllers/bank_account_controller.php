<?php

session_start();
$_SESSION['account_owner'] = $_POST['account_owner'];
$_SESSION['iban'] = $_POST['iban'];
//Connecting to a database and insert new info if new session
//or update info if in existing session
$db_conn = mysqli_connect("localhost","root","","wunder");
if (!$db_conn) {
    die(mysqli_connect_error());
}
$sql_query = "";
if (!isset($_SESSION['customer_id']) || empty($_SESSION['customer_id'])) {
    $sql_query = "INSERT INTO CUSTOMER (first_name,last_name,telephone," .
                "street,house_number,zip_code,city,account_owner,iban) " .
                "VALUES ('" . $_SESSION['first_name'] . "','" . $_SESSION['last_name'] . "','" . $_SESSION['telephone'] . "','" .
                $_SESSION['street'] . "','" . $_SESSION['house_number'] . "','" . $_SESSION['zip_code'] . "','" . $_SESSION['city'] . "','" .
                $_SESSION['account_owner'] . "','" . $_SESSION['iban'] . "')";
}
else {
    $sql_query = "UPDATE CUSTOMER SET first_name=" . "'" . $_SESSION['first_name'] . "', " . 
                    "last_name='" . $_SESSION['last_name'] . "', " .
                    "telephone='" . $_SESSION['telephone'] . "', " .
                    "street='" . $_SESSION['street'] . "', " .
                    "house_number='" . $_SESSION['house_number'] . "', " .
                    "zip_code='" . $_SESSION['zip_code'] . "', " . 
                    "city='" . $_SESSION['city'] . "', " .
                    "account_owner='" . $_SESSION['account_owner'] . "', " .
                    "iban='" . $_SESSION['iban'] . "', " .
                    "WHERE id=" . $_SESSION['customer_id']; 
}

//IF THERE'S AN ERROR WITH THE DATABASE REPORT THE ERROR ON UI AND EXIT
if ($db_conn->query($sql_query) === FALSE) {
    $err = $db_conn->error;
    mysqli_close($db_conn);
    die($err);
}

//NOW EVERYTHING SEEMS FINE SO I WILL CONSTRUCT A READY OBJECT FOR CONNECTING TO YOUR
//ENDPOINT AND ECHO IT IN RESPONSE. THE KEY PART IS THE CUSTOMER_ID WHICH I WILL
//RETRIEVE FROM THE DATABASE IN CASE OF A NEW SESSION
if (!isset($_SESSION['customer_id']) || empty($_SESSION['customer_id'])) {
    $sql_query = "SELECT id FROM CUSTOMER WHERE iban=" . "'" . $_SESSION['iban'] . "'";
    $result = $db_conn->query($sql_query);
    if ($result->num_rows == 0) {
        mysqli_close($db_conn);
        die("There is not custommer with the specified ID. Please try again!!!");
    }
    $_SESSION['customer_id'] = $result->fetch_assoc()["id"];
}
mysqli_close($db_conn);
echo json_encode(
    array('customerId' => $_SESSION['customer_id'], 'iban' => $_SESSION['iban'], 'owner' => $_SESSION['account_owner'])
);


?>