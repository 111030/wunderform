<?php

session_start();
$db_conn = mysqli_connect("localhost","root","","wunder");
if (!$db_conn) {
    die(mysqli_connect_error());
}

$sql_query = "DELETE FROM CUSTOMER WHERE id=" . $_POST['customer_id'];
if ($db_conn->query($sql_query) === FALSE) {
    $error = $db_conn->error();
    mysqli_close($db_conn);
    die($error);
}
mysqli_close($db_conn);
unset($_SESSION['customer_id']);
echo $_POST['error'] . ". <br/>The customer with id " . $_POST['customer_id'] . " was removed from the database."

?>