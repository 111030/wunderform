<?php

session_start();
if (!isset($_SESSION['page_number'])) {
    $_SESSION['page_number'] = 0;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>Registration</title>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>
<body>
    <div id="overlay" style="display: none;">
        <div id="spinner"></div>
    </div>
    <div class="container">
        <div class="container-center col-lg-4">
            <form action="" method="post" id="toggleable_form_1">
                <div class="form-group">
                    <label for="first_name">Firstname:</label>
                    <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo isset($_SESSION['first_name']) ? $_SESSION['first_name'] : ''; ?>" required>
                </div>
                <div class="form-group">
                    <label for="last_name">Lastname:</label>
                    <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo isset($_SESSION['last_name']) ? $_SESSION['last_name'] : ''; ?>" required>
                </div>
                <div class="form-group">
                    <label for="telephone">Telephone:</label>
                    <input type="text" class="form-control" name="telephone" id="telephone" value="<?php echo isset($_SESSION['telephone']) ? $_SESSION['telephone'] : ''; ?>" required>
                </div>
                <button type="button" class="btn btn-success btn-next">Next</button>
                <p id="error_1" style="display: none;"></p>            
            </form>
            <form action="" method="post" id="toggleable_form_2">
                <div class="form-group">
                    <label for="street">Street:</label>
                    <input type="text" class="form-control" name="street" id="street" value="<?php echo isset($_SESSION['street']) ? $_SESSION['street'] : ''; ?>" required>
                </div>
                <div class="form-group">
                    <label for="house_number">House number:</label>
                    <input type="number" class="form-control" name="house_number" id="house_number" value="<?php echo isset($_SESSION['house_number']) ? $_SESSION['house_number'] : ''; ?>" reqired>
                </div>
                <div class="form-group">
                    <label for="zip_code">Zip:</label>
                    <input type="number" class="form-control" name="zip_code" id="zip_code" value="<?php echo isset($_SESSION['zip_code']) ? $_SESSION['zip_code'] : ''; ?>" required>
                </div>
                <div class="form-group">
                    <label for="city">City:</label>
                    <input type="text" class="form-control" name="city" id="city" value="<?php echo isset($_SESSION['city']) ? $_SESSION['city'] : ''; ?>" required>
                </div>
                <button type="button" class="btn btn-danger btn-previous">Previous</button>
                <button type="button" class="btn btn-success btn-next">Next</button>
                <p id="error_2" style="display: none;"></p>              
            </form>
            <form action="" method="post" id="toggleable_form_3">
                <div class="form-group">
                    <label for="account_owner">Owner:</label>
                    <input type="text" class="form-control" name="account_owner" id="account_owner" value="<?php echo isset($_SESSION['account_owner']) ? $_SESSION['account_owner'] : ''; ?>" required>
                </div>
                <div class="form-group">
                    <label for="iban">IBAN:</label>
                    <input type="text" class="form-control" name="iban" id="iban" value="<?php echo isset($_SESSION['iban']) ? $_SESSION['iban'] : ''; ?>" required>
                </div>
                <button type="button" class="btn btn-danger btn-previous">Previous</button>
                <button type="button" class="btn btn-success btn-next">Finish</button>
                <p id="error_3" style="display: none;"></p>              
            </form>
            <div id="toggleable_form_4">
                <?php echo "SUCCESS!!!"?>   
            </div>
    </div>
</div>
    
<script src="js/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="js/ajax.js"></script>
<script>
    var selected = 0;
    const NUM_STEPS = 4;

    $(function() {
        
        ajaxGetSessionState('controllers/session_getter.php');

        $(".btn-next").click(goNext);
        $(".btn-previous").click(goBack);

        function goNext() {
            sendAjaxToSelected();
        }

        function goBack() {
            ajaxPostSessionState('controllers/session_setter.php', {'page_number':--selected});
        }

        function sendAjaxToSelected() {
            switch(selected) {
                case "1": sendAjaxPost("controllers/personal_info_controller.php", 
                        {"first_name":$("#first_name").val(),"last_name":$("#last_name").val(),"telephone":$("#telephone").val()},
                        selected);
                        break;
                case "2": sendAjaxPost("controllers/street_address_controller.php", 
                        {"street":$("#street").val(),"house_number":$("#house_number").val(),"zip_code":$("#zip_code").val(),"city":$("#city").val()},
                        selected);
                        break;
                case "3": sendAjaxPost("controllers/bank_account_controller.php", 
                        {"account_owner":$("#account_owner").val(),"iban":$("#iban").val()},
                        selected);
                        break;
                default:
                        $("#overlay").toggle(false);
                        break;
            }
        }
    });
    
</script>
</body>
</html>