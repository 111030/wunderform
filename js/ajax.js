function sendAjaxPost(url, params, index) {
    $.ajax({
        beforeSend: function(jqXHR, settings) {
            $("#overlay").toggle(true);
        },
        data: params,
        error: function(jqXHR, textStatus, errorThrown) {
            $("#overlay").toggle(false);
            $("#error_" + index).toggle(true);
            $("#error_" + index).text(errorThrown);
        },
        method: "POST",
        success: function(data, textStatus, jqXHR) {
            $("#overlay").toggle(false);
            $("#error_" + index).toggle(false);
            $("#error_" + index).text("");
            if (selected == 3) {
                $.ajax({
                    beforeSend: function(jqXHR, settings) {
                        $("#overlay").toggle(true);
                    },
                    crossDomain: true,
                    data: JSON.stringify(JSON.parse(data)),
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#overlay").toggle(false);
                        let jsonData = JSON.parse(data);
                        ajaxDeleteFromDatabaseById("controllers/delete_from_customer_db.php", {"customer_id":jsonData["customerId"],"error":textStatus + " - " + errorThrown}, index);
                    },
                    headers: {"Access-Control-Allow-Origin": "*"},
                    method: 'POST',
                    success: function(dataPayment, textStatus, jqXHR) {
                        $("#overlay").toggle(false);
                        $("#error_" + index).toggle(false);
                        $("#error_" + index).text("");
                        let jsonData = JSON.parse(data);
                        destroySessionAndFinish("controllers/update_and_finish.php", {"customerId":jsonData["customerId"],"paymentDataId":dataPayment["paymentDataId"]}, index);
                    },
                    url: "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data"
                });
            }
            else {
                ajaxPostSessionState('controllers/session_setter.php', {"page_number": ++selected});
            }
        },
        url: url
    });
}

function ajaxGetSessionState(url) {
    $.ajax({
        async: false,
        method: "GET",
        success: function(data, textStatus, jqXHR) {
            selected = data;
            for (var i=1; i<=NUM_STEPS; i++) {
                if (i == selected)
                    $("#toggleable_form_" + i).toggle(true);
                else
                    $("#toggleable_form_" + i).toggle(false);
            }
        },
        url: url
    });
}

function ajaxPostSessionState(url, state) {
    $.ajax({
        data: state,
        method: "POST",
        success: function(data, textStatus, jqXHR) {
            selected = data;
            for (var i=1; i<=NUM_STEPS; i++) {
                if (i == selected)
                    $("#toggleable_form_" + i).toggle(true);
                else
                    $("#toggleable_form_" + i).toggle(false);
            }
        },
        url: url
    })
}

function ajaxDeleteFromDatabaseById(url, data, index) {
    $.ajax({
        beforeSend: function(jqXHR, settings) {
            console.log(data);
        },
        data: data,
        error: function(jqXHR, testStatus, errorThrown) {
            $("#error_" + index).toggle(true);
            $("#error_" + index).text(errorThrown + " - " + testStatus);
        },
        method: "POST",
        success: function(data, textStatus, jqXHR) {
            $("#error_" + index).toggle(true);
            $("#error_" + index).text(data);
        },
        url: url
    })
}

function destroySessionAndFinish(url, data, index) {
    $.ajax({
        data: data,
        error: function(jqXHR, testStatus, errorThrown) {
            $("#error_" + index).toggle(true);
            $("#error_" + index).text(errorThrown + " - " + testStatus);
        },
        method: "POST",
        success: function(data, testStatus, jqXHR) {
            ajaxPostSessionState('controllers/session_setter.php', {"page_number": ++selected});
            $("#toggleable_form_4").html("You registered successfully<br/>Your id of the payment is " . data);
        },
        url: url
    });
}